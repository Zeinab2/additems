const initState = {
  items: [
    { id: "1", name: "ahmed", age: "25" },
    { id: "2", name: "mohammed", age: "24" },
    { id: "3", name: "ali", age: "30" }
  ]
};
const reducer = (state = initState, action) => {
  if (action.type == "DELETE") {
    const items = state.items.filter(item => {
      return item.id !== action.text;
    });
    return { items };
    //console.log(action.text);
  } else if (action.type == "ADD") {
    return { items: state.items.concat(action.payload) };

    console.log(action.payload);
  }
  return state;
};
export default reducer;
