import React, { Component } from "react";
import logo from "./logo.svg";
import Items from "./components/Items";
import Form from "./components/Forms";
import Select from "./components/Select";
import Todoitems from "./components/Todo/components/TodoItems/Todoitems";
import Additems from "./components/Todo/components/AddItems/Additems";
import reducer from "./Reducer/Reducer.js";
import { Provider } from "react-redux";
import { createStore } from "redux";

import "./App.css";
const store = createStore(reducer);

class App extends Component {
  state = {
    items: [{ id: "1", name: "ahmed", age: "25" }]
  };

  render() {
    return (
      <div className="App">
        {/* basic way to pass props from parent to child */}
        {/* <Items items={this.state.items} />
        <Form />
        <Select /> */}
        <Provider store={store}>
          <Todoitems />
          <Additems />
        </Provider>
      </div>
    );
  }
}

export default App;
