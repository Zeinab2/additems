import React, { Component } from "react";
import { connect } from "react-redux";
import "./style.css";

function mapStateToProps(state) {
  return {
    items: state.items
  };
}
function mapDispatchToProps(dispatch) {
  return {
    delete: id => dispatch({ type: "DELETE", text: id })
  };
}
class Todoitems extends Component {
  // handleDelete = id => {
  //   // let items = this.state.items;
  //   // let i = items.findIndex(item => item.id === id);
  //   // items.splice(i, 1);
  //   // this.setState({ items });
  //   //by filter function
  //   let items = this.state.items.filter(item => {
  //     return item.id !== id;
  //   });
  //   this.setState({ items });
  // };

  render() {
    return (
      <div>
        {this.props.items.length !== 0 ? (
          <table className="table table-bordered">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Age</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {this.props.items.map(item => {
                return (
                  <tr key={item.id}>
                    <th scope="row">{item.id}</th>
                    <td>{item.name}</td>
                    <td>{item.age}</td>
                    <td
                      className="action"
                      onClick={() => this.props.delete(item.id)}
                    >
                      &times;
                    </td>
                    {/* arrow function prevent fire function without clicking */}
                  </tr>
                );
              })}
            </tbody>
          </table>
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th scope="col" className="message">
                  There Is no Items To Show
                </th>
              </tr>
            </thead>
          </table>
        )}
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Todoitems);
