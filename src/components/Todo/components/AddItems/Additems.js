import React, { Component } from "react";
import { connect } from "react-redux";

import "./style.css";
function mapStateToProps(state) {
  return {
    items: state.items
  };
}
function mapDispatchToProps(dispatch) {
  return {
    addArticle: article => dispatch(addArticle(article))
  };
}
function addArticle(payload) {
  return { type: "ADD", payload };
}
class Additems extends Component {
  // handleSubmit = e => {
  //   e.preventDefault();
  //   console.log(this.state);
  // };
  state = {
    title: [{ id: "3", name: "ali", age: "30" }]
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    const { title } = this.state;
    this.props.addArticle({ title });
    this.setState({ title });
  };

  render() {
    const { title } = this.state;
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Enter your name"
            id="name"
            onChange={this.handleChange}
          />
          <input
            type="number"
            placeholder="Enter your age"
            id="age"
            onChange={this.handleChange}
          />
          <input type="submit" value="Add" />
        </form>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Additems);
