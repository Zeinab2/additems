import React, { Component } from "react";

class Items extends Component {
  render() {
    // const id = this.props.id;
    // const name = this.props.name;
    // const age = this.props.age;
    // const { id, name, age } = this.props;
    ////show data from state inside parent component
    const { items } = this.props;
    const theItems = items.map(item => {
      return (
        <div>
          <p>{item.id}</p>
          <p>{item.name}</p>
          <p>{item.age}</p>
        </div>
      );
    });

    return (
      <div>
        {/* first way */}
        {/* <p>{this.props.id}</p>
        <p>{this.props.name}</p>
        <p>{this.props.age}</p> */}
        {/* second way */}
        {/* <p>{id}</p>
        <p>{name}</p>
        <p>{age}</p> */}
        {theItems}
      </div>
    );
  }
}
export default Items;
