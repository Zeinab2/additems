import React, { Component } from "react";

class Form extends Component {
  state = {
    name: ""
  };
  //use onChange event to return what write inside input
  handleChange = e => {
    //use arrow function to easy the usage of this
    this.setState({
      name: e.target.value
    });
  };
  //use handleSubmit inside form to print input by enter and button
  handleSubmit = e => {
    e.preventDefault(); //stop refresh of page
    console.log(this.state.name);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input onChange={this.handleChange} />
        <button>Submit</button>
        <p>{this.state.name}</p>
      </form>
    );
  }
}
export default Form;
