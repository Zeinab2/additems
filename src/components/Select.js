import React, { Component } from "react";
class Select extends Component {
  state = {
    name: ""
  };
  handleChange = e => {
    this.setState({
      name: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state.name);
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <select onChange={this.handleChange}>
            <option value="value1">value1</option>
            <option value="value2">value2</option>
            <option value="value3">value3</option>
          </select>
          <input type="submit" value="send" />
          <p>{this.state.name}</p>
        </form>
      </div>
    );
  }
}
export default Select;
